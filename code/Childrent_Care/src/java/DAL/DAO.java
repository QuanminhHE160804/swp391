/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import DTO.User;
import com.mysql.cj.protocol.Resultset;
import com.sun.javafx.collections.VetoableListDecorator;
import context.DBContext;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Grey
 */
public class DAO {
    
    Connection conn = null;
    PreparedStatement ps = null;
    Resultset set = null;
    
    public DAO() {
        DBContext dBContext = new DBContext();
        try {
            conn = DBContext.getJDBCConnection();
            System.out.println("Thanh cong!");
        } catch (Exception e) {
            System.out.println("error: " + e);
        }
    }
    
    public void createNewUser(User user){
        try {
            String sql = "insert into [users]\n"
                    + "([users_id]\n"
                    + ",[users_passwords]\n"
                    + ",[users_phonenumber]\n"
                    + ",[users_gmail]\n"
                    + ",[users_address]\n"
                    + ",[ users_gender]\n"
                    + ",[users_fullname]\n"
                    + ",[users_dob]\n"
                    + ",[rollnumber])\n"
                    + "     VALUES (?,?,?,?,?,?,?,?,?)";
            
            conn = (Connection) new DBContext();
            
            ps = conn.prepareStatement(sql);
            ps.setString(1, user.getUsers_id());
            ps.setString(2, user.getUsers_passwords());
            ps.setString(3, user.getUsers_phonenumber());
            ps.setString(4, user.getUsers_gmail());
            ps.setString(5, user.getUsers_address());
            ps.setString(6, user.getUsers_gender());
            ps.setString(7, user.getUsers_fullname());
            ps.setDate(8, (Date) user.getUsers_dob());
            ps.setInt(9, user.getRollnumber());
            
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    private List<User> listUser;

    public List<User> loadAllUser() {
        listUser = new ArrayList<User>();
        String sql = "SELECT * FROM users ";
        try {
            
            conn = DBContext.getJDBCConnection();
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                //acc.add(new a(rs.getString(1), rs.getString(2), rs.getString(3)));
                listUser.add(new User(rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getString(6),
                rs.getString(7),
                rs.getDate(8),
                rs.getInt(9)));
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
        return listUser;
    }
    
    public boolean login(String u, String p) {
         DAO dao = new DAO();
        List<User> listUser =  dao.loadAllUser();
        for (User a : listUser) {
            if (a.getUsers_id().equals(u) && a.getUsers_passwords().equals(p)) {
                return true;
            }
        }
        return false;
    }

    public List<User> getListUser() {
        return listUser;
    }

    public void setListUser(List<User> listUser) {
        this.listUser = listUser;
    }

    
    
    
    public static void main(String[] args) {
        DAO dao = new DAO();
        List<User> l =  dao.loadAllUser();
        for (User user : l) {
            System.out.println(user.toString());
        } 
    }
    
}
