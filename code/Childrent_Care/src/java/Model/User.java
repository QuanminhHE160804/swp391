/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DTO;


import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class User {
    private String users_id;
    private String users_passwords;
    private String users_phonenumber;
    private String users_gmail;
    private String users_address;
    private String users_gender;
    private String users_fullname;
    private Date users_dob;
    private int rollnumber;

    public User() {
    }

    public User(String users_id, String users_passwords, String users_phonenumber, String users_gmail, String users_address, String users_gender, String users_fullname, Date users_dob, int rollnumber) {
        this.users_id = users_id;
        this.users_passwords = users_passwords;
        this.users_phonenumber = users_phonenumber;
        this.users_gmail = users_gmail;
        this.users_address = users_address;
        this.users_gender = users_gender;
        this.users_fullname = users_fullname;
        this.users_dob = users_dob;
        this.rollnumber = rollnumber;
    }

    
//    public User(String users_id, String users_passwords, String users_phonenumber, String users_gmail, String users_address, String users_gender, String users_fullname, String users_dob, int rollnumber) {
//        this.users_id = users_id;
//        this.users_passwords = users_passwords;
//        this.users_phonenumber = users_phonenumber;
//        this.users_gmail = users_gmail;
//        this.users_address = users_address;
//        this.users_gender = users_gender;
//        this.users_fullname = users_fullname;
//        setUsers_dob(users_dob);
//        this.rollnumber = rollnumber;
//    }

    public String getUsers_id() {
        return users_id;
    }

    public void setUsers_id(String users_id) {
        this.users_id = users_id;
    }

    public String getUsers_passwords() {
        return users_passwords;
    }

    public void setUsers_passwords(String users_passwords) {
        this.users_passwords = users_passwords;
    }

    public String getUsers_phonenumber() {
        return users_phonenumber;
    }

    public void setUsers_phonenumber(String users_phonenumber) {
        this.users_phonenumber = users_phonenumber;
    }

    public String getUsers_gmail() {
        return users_gmail;
    }

    public void setUsers_gmail(String users_gmail) {
        this.users_gmail = users_gmail;
    }

    public String getUsers_address() {
        return users_address;
    }

    public void setUsers_address(String users_address) {
        this.users_address = users_address;
    }

    public String getUsers_gender() {
        return users_gender;
    }

    public void setUsers_gender(String users_gender) {
        this.users_gender = users_gender;
    }

    public String getUsers_fullname() {
        return users_fullname;
    }

    public void setUsers_fullname(String users_fullname) {
        this.users_fullname = users_fullname;
    }

//    public String getUsers_dob() {
//        SimpleDateFormat sd= new SimpleDateFormat("yyyy/MM/dd");
//        sd.setLenient(true);
//        return sd.format(users_dob);
//    }
//
//    public void setUsers_dob(String users_dob) {
//        SimpleDateFormat sd= new SimpleDateFormat("yyyy/MM/dd");
//        sd.setLenient(true);
//        try {
//            this.users_dob= new Date(sd.parse(users_dob).getTime());
//        } catch (ParseException ex) {
//            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
//            throw new RuntimeException("Wrong data format!!!");
//        }
//    }

    public Date getUsers_dob() {
        return users_dob;
    }

    public void setUsers_dob(Date users_dob) {
        this.users_dob = users_dob;
    }
    

    public int getRollnumber() {
        return rollnumber;
    }

    public void setRollnumber(int rollnumber) {
        this.rollnumber = rollnumber;
    }

    @Override
    public String toString() {
        return "User{" + "users_id=" + users_id + ", users_passwords=" + users_passwords + ", users_phonenumber=" + users_phonenumber + ", users_gmail=" + users_gmail + ", users_address=" + users_address + ", users_gender=" + users_gender + ", users_fullname=" + users_fullname + ", users_dob=" + users_dob + ", rollnumber=" + rollnumber + '}';
    }
    
    
}
